﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Timer : MonoBehaviour
{
	public Text counterText;
	GameManager gameManager;

	public float seconds, minutes;
	private float leftOverTime = float.MaxValue;
	// Use this for initialization
	void Start()
	{
		gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
		counterText = GetComponent<Text>() as Text;
	}

	public float GetLeftOverTime() { return leftOverTime; }

	// Update is called once per frame
	void Update()
	{
		leftOverTime = gameManager.CurrentLevelTime - Time.timeSinceLevelLoad;

		if (leftOverTime > 0)
		{
			minutes = (int)(leftOverTime / 60f);
			seconds = (int)(leftOverTime % 60f);
			counterText.text = minutes.ToString("00") + ":" + seconds.ToString("00");
		}
	}
}