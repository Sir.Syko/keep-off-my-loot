﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootChest : MonoBehaviour {

    private int currentLoot = 0;
    public GameObject chestCoin;

    public void updateLoot(int number)
    {
        while (currentLoot < number && currentLoot < 150)
        {
            currentLoot++;
            Vector3 offset = getSpawnPosition();
            GameObject obj = Instantiate(chestCoin, transform.position, transform.rotation) as GameObject;
			obj.transform.parent = transform;
            obj.transform.Translate(offset);
        }

        while (currentLoot > number)
        {
            if (currentLoot < 150)
            {
                GameObject coin = GameObject.FindWithTag("ChestCoin");
                Destroy(coin);
            }

            currentLoot--;
        }
    }

    public void init(int number)
    {
        currentLoot = number;
    }

    private Vector3 getSpawnPosition()
    {
        return new Vector3(Random.Range(-0.4f, 0.4f), Random.Range(0.045f, 0.055f), Random.Range(-0.12f, 0.17f));
    }
}
