﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleEffect : MonoBehaviour {

	public float LifeTime = 1f;

	float currentLifeTime = 0;

	// Update is called once per frame
	void Update () {
		currentLifeTime += Time.deltaTime;
		if (currentLifeTime > LifeTime)
			Destroy(transform.parent.gameObject);
	}
}
