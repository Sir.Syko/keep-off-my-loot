﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	GameObject player;
	PlayerInfo playerInfo;
	int currentSceneId;
	public float CurrentLevelTime;
	public bool InstantiateMap = true;

	public PlayerInfo GetPlayerInfo()
	{
		return playerInfo;
	}

	private void OnEnable()
	{
		currentSceneId = SceneManager.GetActiveScene().buildIndex;
		player = GameObject.Find("Player");
		playerInfo = player.GetComponent<PlayerInfo>();
		InitializeLevel();
	}

	// Use this for initialization
	void Start () {
		
	}

	public void Reset()
	{
		player.transform.position = new Vector3(0f, 0f, 0f);
		playerInfo.Reset();
	}

	public void InitializeLevel()
	{
		//Get the current time
		Type level = Type.GetType("Level" + currentSceneId);
		var fieldInfo = level.GetField("Time");
		CurrentLevelTime = (float)fieldInfo.GetValue(null);

		Texture2D levelTexture = Resources.Load("Maps/Level" + currentSceneId) as Texture2D;
		if (InstantiateMap)
		{
			//var gameObjects

			var curPosition = new Vector3(0f, 0f, 0f);
			for (int x = 0; x < levelTexture.width; x++)
			{
				for (int z = 0; z < levelTexture.height; z++)
				{
					curPosition = new Vector3(x, 0f, z);
					var pixel = levelTexture.GetPixel(x, z);

					/*if (pixel == Color.red)
					{
						Instantiate(Resources.Load("Prefabs/MeleeSpawn"), curPosition, Quaternion.identity);
					}

					if (pixel == Color.green)
					{
						Instantiate(Resources.Load("Prefabs/RangedSpawn"), curPosition, Quaternion.identity);
					}*/

					if (pixel == Color.white)
					{
						Instantiate(Resources.Load("Prefabs/Wall"), curPosition, Quaternion.identity);
					}

					if (pixel == Color.red)
					{
						Instantiate(Resources.Load("Prefabs/HighWall"), curPosition, Quaternion.identity);
					}

					/*if (pixel == Color.black)
					{
						var terrainTile = Instantiate(Resources.Load("Prefabs/terrainTile"), curPosition, Quaternion.identity) as GameObject;
						terrainTile.transform.localScale = new Vector3(0.5f, UnityEngine.Random.Range(0.01f, 0.2f), 0.5f);
					}*/
				}
			}
		}

		//Center the player
		player.transform.position = new Vector3(levelTexture.width * 0.5f, player.transform.position.y, levelTexture.height * 0.5f);

		//Position the camera
		Camera.main.transform.position = player.transform.position + Camera.main.transform.position;
	}

	// Update is called once per frame
	void Update () {
		
	}

	void FixedUpdate()
	{
		
	}

	public int CurrentLevel()
	{
		return currentSceneId;
	}

	public int NextLevel()
	{
		var levelCount = SceneManager.sceneCount - 1;
		return currentSceneId + 1;
	}
}
