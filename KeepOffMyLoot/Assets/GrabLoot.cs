﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabLoot : MonoBehaviour {

	PlayerInfo playerInfo;

	// Use this for initialization
	void Start () {
		playerInfo = transform.GetComponent<PlayerInfo>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter(Collision collision)
	{
		ContactPoint contact = collision.contacts[0];
		Quaternion rot = Quaternion.FromToRotation(Vector3.up, contact.normal);
		Vector3 pos = contact.point;

		if (collision.transform.tag.CompareTo("Loot") == 0)
		{
			var lootScrpt = collision.transform.GetComponent<Loot>();
			playerInfo.GrabLoot(lootScrpt.Value);
			Destroy(collision.gameObject);
		}
	}
}
