﻿using UnityEngine;
  #if !UNITY_5
    using UnityEngine.AI;
  #endif
using System.Collections;

public class PathAgent : MonoBehaviour
{
	public Transform target;
	protected float speed = 2f;
	public float range = 3f;
	public bool StayAtRange = false;
    public bool moveToRange = false;
    protected NavMeshAgent agent;
    protected bool endlessFollow = true;
    protected Vector3 temporaryTarget;
    protected Vector3 startingPos;
    protected NavMeshPath currentPath;
    protected int indexCurNode = 0;
    protected float elapsed;
    protected float originY;
    protected bool initialized = false;

	void Start()
	{
		gameObject.GetComponent<InterpolatedTransformUpdater>().enabled = true;
		gameObject.GetComponent<InterpolatedTransform>().enabled = true;
		currentPath = new NavMeshPath();
		
		var gameObj = GameObject.Find("Player");
		if (gameObj == null)
		{
			this.gameObject.SetActive(false);
			return;
		}
		target = gameObj.transform;
		// Target for the moment is always the player
		
		agent = GetComponent<NavMeshAgent>();
		agent.Stop();
		// Disabling auto-braking allows for continuous movement
		// between points (ie, the agent doesn't slow down as it
		// approaches a destination point).
		agent.autoBraking = false;
		startingPos = temporaryTarget = transform.position;
		originY = temporaryTarget.y;
		GotoToTarget();
		agent.Warp(transform.position);
	}

	void GotoToTarget()
	{
		if (target == null)
			return;

		float distance = (target.position - transform.position).magnitude;

		var curTemporaryTarget = temporaryTarget;

		// Set the agent to go to the currently selected destination.
		if (distance <= range || endlessFollow)
		{
			endlessFollow = true;
			if (StayAtRange)
			{
				temporaryTarget = target.position - (target.position - transform.position).normalized * range;
			}
			else
			{
				temporaryTarget = target.position;
			}
		}
		else
		{
			if ((temporaryTarget- transform.position).magnitude <= 1.1f)
			{
				//Move randomly
				temporaryTarget = transform.position + new Vector3(Random.Range(-3f, 3f), 0f, Random.Range(-3f, 3f));
			}
		}

		NavMeshHit hit;
		if (NavMesh.SamplePosition(temporaryTarget, out hit, 1f, NavMesh.AllAreas))
			temporaryTarget = new Vector3(hit.position.x, originY, hit.position.z);
		else temporaryTarget = curTemporaryTarget;

		NavMesh.SamplePosition(transform.position, out hit, 1f, NavMesh.AllAreas);
		var closestCurrentPosition = new Vector3(hit.position.x, originY, hit.position.z);

		var calculatePath = false;
		if (elapsed > 0.4f)
		{
			elapsed -= 0.4f;
			calculatePath = true;
		}

		if (temporaryTarget == curTemporaryTarget && elapsed > 1f)
			calculatePath = false;

		if (calculatePath)
		{
			indexCurNode = 0;
			if (!NavMesh.CalculatePath(closestCurrentPosition, temporaryTarget, NavMesh.AllAreas, currentPath))
			{
				if(currentPath.status == NavMeshPathStatus.PathComplete || currentPath.status == NavMeshPathStatus.PathInvalid)
					temporaryTarget = curTemporaryTarget;
			}
		}
	}

    public void UpdateSpeed(float baseSpeed, float speedModifier)
    {
        speed = baseSpeed + speedModifier;
    }

    void FixedUpdate()
	{

		elapsed += Time.deltaTime;
		// Choose the next destination point when the agent gets
		// close to the current one.

		GotoToTarget();

		if (currentPath != null)
		{
			var corners = currentPath.corners;
			if (indexCurNode < corners.Length)
			{
				var nextPos = corners[indexCurNode];
				Vector3 direction = (nextPos - transform.position);
				direction = new Vector3(direction.x, 0f, direction.z).normalized;

				var originalY = transform.position.y;
				//var tmp = SuperSmoothLerp(transform.position, transform.position, nextPos, Time.fixedDeltaTime, speed);

				var tmp = Vector3.MoveTowards(transform.position, transform.position + direction * 3.0f, 0.1f * speed);

				//var tmp = Vector3.MoveTowards(transform.position, nextPos, 0.1f * speed);
				transform.position = new Vector3(tmp.x, originalY, tmp.z);
				
				if(direction.magnitude > 0f)
					transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(direction, Vector3.up), 10);
				if ((corners[indexCurNode] - transform.position).magnitude < 0.8f)
				{
					indexCurNode++;
				}
			}
		}
	}
}