﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInfo : MonoBehaviour {

    public int player_health = 100;
	public int player_loot = 0;

    private int startingHealth;
	private int startingLoot;
	private PlayerHit playerHit;

	// Use this for initialization
	void Start () {
		startingHealth = player_health;
		startingLoot = player_loot;
		playerHit = gameObject.GetComponent<PlayerHit>();
    }

	public bool IsDead() { return player_health <= 0; }
    public int Health() { return player_health; }
	public int Loot() { return player_loot; }

    public void hurtPlayer(int damage)
    {
        player_health -= damage;
		playerHit.blink();
		if (player_health < 0)
		{
			player_health = 0;
			//TODO DEATH
			this.gameObject.SetActive(false);
		}
	}

	public void GrabLoot(int value)
	{
        player_loot += value;
    }

    public void LoseLoot(int value)
    {
        player_loot -= value;
    }

    public void LeaveLoot()
	{
		player_loot--;
		if (player_loot < 0)
			player_loot = 0;
	}

	public void Reset()
	{
		player_health = startingHealth;
		player_loot = startingLoot;
	}

	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.K))
        {
            hurtPlayer(Random.Range(1,10));
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            GrabLoot(Random.Range(1, 10));
        }
    }
}
