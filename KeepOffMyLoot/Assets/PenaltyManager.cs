﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class PenaltyManager : MonoBehaviour {

    GameObject player;
	GameObject pivot;
	PlayerInfo playerInfo;

    private EnemyManager enemyManager;

    public int max_relevant_loot = 100;
    public GameObject chest_prefab;
    public GameObject chest_puff;
	private LootChest lootChest;
    private LootChest lootChest2;

    private int[] penalty_threshold_list;
    
    private int splitChance = 0;
    private int enemyMutateNumber = 0;
    private float enemyMutateBaseTimer = 10f;
    private float enemyMutateTimer = 10f;

    private int current_penalty = 0;

    // Use this for initialization
    void Start ()
    {
        player = GameObject.Find("Player");
		pivot = player.transform.Find("Pivot").gameObject;
        playerInfo = player.GetComponent<PlayerInfo>();
        penalty_threshold_list = new int[] { Mathf.RoundToInt(max_relevant_loot / 3.0f), Mathf.RoundToInt(max_relevant_loot / 2.0f), Mathf.RoundToInt(max_relevant_loot * 3.0f / 4.0f), Mathf.RoundToInt(max_relevant_loot * 9.0f / 10.0f) };
        enemyManager = GameObject.FindWithTag("GameController").GetComponent<EnemyManager>();
    }

    private void increasePenalty(int _new_penalty)
    {
        current_penalty++;
        if(current_penalty == 1)
        {
            GameObject chest = Instantiate(chest_prefab, pivot.transform.position, pivot.transform.rotation) as GameObject;
			chest.transform.parent = pivot.transform;
            //Instantiate(chest_puff, pivot.transform.position - pivot.transform.forward * 0.8f, pivot.transform.rotation, pivot.transform);
            lootChest = chest.GetComponent<LootChest>();
            lootChest.init(playerInfo.Loot());
            chest.transform.position = pivot.transform.position - pivot.transform.forward * 0.8f;
        }
        if (current_penalty == 2)
        {
            splitChance += 10;
        }
        if (current_penalty == 3)
        {
            GameObject chest = Instantiate(chest_prefab, pivot.transform.position, pivot.transform.rotation) as GameObject;
			chest.transform.parent = pivot.transform;
            lootChest2 = chest.GetComponent<LootChest>();
            lootChest2.init(playerInfo.Loot());
            chest.transform.position = pivot.transform.position - pivot.transform.forward * 1.35f;
            splitChance += 10;
            enemyMutateNumber += 1;
        }
        if (current_penalty == 4)
        {
            splitChance = 10;
            enemyMutateNumber += 1;
            enemyMutateBaseTimer /= 2;
        }

        enemyManager.IncreasePenalty();
    }

    private void decreasePenalty(int _new_penalty)
    {
        current_penalty--;
        if (current_penalty == 3)
        {
            splitChance -= 10;
            enemyMutateBaseTimer *= 2;
            enemyMutateNumber -= 1;
        }
        if (current_penalty == 2)
        {
            Destroy(lootChest2.gameObject);
            lootChest2 = null;
            splitChance -= 10;
            enemyMutateNumber -= 1;
        }
        if (current_penalty == 1)
        {
            enemyMutateNumber = 0;
            splitChance = 0;
        }
        if (current_penalty == 0)
        {
            Destroy(lootChest.gameObject);
            lootChest = null;
        }

        enemyManager.DecreasePenalty();
    }

    public int EnemySplitChance()
    {
        return splitChance;
    }

    public bool EnemiesMutating()
    {
        return enemyMutateNumber > 0;
    }

    private int get_current_penalty()
    {
        int index = 0;
        foreach (int penalty in penalty_threshold_list)
        {
            if (playerInfo.Loot() < penalty)
            {
                break;
            }

            index++;
        }

        return index;
    }

    private void EnemyMutate()
    {
        List<GameObject> enemy_list = new List<GameObject>(GameObject.FindGameObjectsWithTag("Enemy"));
        enemy_list = Shuffle(enemy_list);

        int numMutations = 0;
        foreach (var enemy in enemy_list)
        {
            Enemy enemyComp = enemy.GetComponent<Enemy>();
            
            //HACK
            if (enemyComp == null)
                continue;

            if (enemyComp.Mutate()) numMutations++;

            if (numMutations >= enemyMutateNumber) break;
        }
    }

    // Update is called once per frame
    void FixedUpdate ()
    {
        int new_penalty = get_current_penalty();
        if (new_penalty > current_penalty) increasePenalty(new_penalty);
        if (new_penalty < current_penalty) decreasePenalty(new_penalty);

        if (lootChest2 != null) lootChest2.updateLoot(playerInfo.Loot());
        else if (lootChest != null)  lootChest.updateLoot(playerInfo.Loot());

        if(EnemiesMutating())
        {
            enemyMutateTimer -= Time.fixedDeltaTime;

            if (enemyMutateTimer <= 0)
            {
                EnemyMutate();
                enemyMutateTimer = enemyMutateBaseTimer;
            }
        }
    }

    public List<GameObject> Shuffle(List<GameObject> list)
    {
        int n = list.Count;
        System.Random rnd = new System.Random();
        while (n > 1)
        {
            int k = (rnd.Next(0, n) % n);
            n--;
            GameObject value = list[k];
            list[k] = list[n];
            list[n] = value;
        }

        return list;
    }
}