﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loot : MonoBehaviour {

	public float LifeTime = 10f;
	public int Value = 1;
	private float currTime = 0;

	void FixedUpdate()
	{
		currTime += Time.fixedDeltaTime;
		if (currTime > LifeTime)
			Destroy(this.gameObject);
	}

}
