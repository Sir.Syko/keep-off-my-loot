﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class VictoryManager : MonoBehaviour {

    public AudioClip starWinSound;
    public AudioClip starLossSound;
    private AudioSource audioSource;

    RawImage star0tex;
	RawImage star1tex;
	RawImage star2tex;
	Text maxTime;
	Text endText;
	Text nextLevelText;

	Texture2D emptyStar;
	Texture2D filledStar;
	PenaltyManager penaltyManager;
	GameManager gameManager;
	PlayerInfo playerInfo;
	Timer timer;

	GameObject Star0;
	GameObject Star1;
	GameObject Star2;
	GameObject Timer;
	GameObject EndLevelText;
	GameObject ExitButton;
	GameObject NextLevelButton;
	GameObject RestartButton;
	public GameObject explosion;

	private int[] star_threshold_list;
	private int current_star = 0;

	// Use this for initialization
	void Start() {
		Star0 = GameObject.Find("Star0");
		Star1 = GameObject.Find("Star1");
		Star2 = GameObject.Find("Star2");
		Timer = GameObject.Find("Timer");
		EndLevelText = GameObject.Find("EndLevelText");
		EndLevelText.SetActive(false);
		ExitButton = GameObject.Find("Exit");
		ExitButton.SetActive(false);
		NextLevelButton = GameObject.Find("NextLevel");
		NextLevelButton.SetActive(false);
		RestartButton = GameObject.Find("Restart");
		RestartButton.SetActive(false);

		endText = EndLevelText.GetComponent<Text>();
		nextLevelText = NextLevelButton.GetComponentInChildren<Text>();

		var player = GameObject.Find("Player");
		playerInfo = player.GetComponent<PlayerInfo>();
		penaltyManager = GetComponent<PenaltyManager>();
		gameManager = GetComponent<GameManager>();
		var lootPerStar = (int)(penaltyManager.max_relevant_loot / 3f);
		star_threshold_list = new int[] { lootPerStar, lootPerStar * 2, penaltyManager.max_relevant_loot };
		star0tex = Star0.GetComponent<RawImage>();
		star1tex = Star1.GetComponent<RawImage>();
		star2tex = Star2.GetComponent<RawImage>();
		emptyStar = Resources.Load("Images/GrayStar") as Texture2D;
		filledStar = Resources.Load("Images/FilledStar") as Texture2D;
		timer = Timer.GetComponent<Timer>();

        audioSource = GetComponent<AudioSource>();
	}

	public void Exit()
	{
		#if (UNITY_EDITOR || DEVELOPMENT_BUILD)
			Debug.Log(this.name + " : " + this.GetType() + " : " + System.Reflection.MethodBase.GetCurrentMethod().Name);
		#endif
		#if (UNITY_EDITOR)
			UnityEditor.EditorApplication.isPlaying = false;
		#elif (UNITY_STANDALONE)
			Application.Quit();
		#elif (UNITY_WEBGL)
			Application.OpenURL("about:blank");
		#endif
	}

	public void Restart()
	{
		SceneManager.LoadScene(gameManager.CurrentLevel());
	}

	public void NextLevel()
	{
		var nextLevel = gameManager.NextLevel();
		if(nextLevel < SceneManager.sceneCountInBuildSettings)
			SceneManager.LoadScene(gameManager.NextLevel());
	}

	private void updateStars()
	{
		if (current_star >= 3)
		{
			star2tex.texture = filledStar;
			star1tex.texture = filledStar;
			star0tex.texture = filledStar;
		}
		else if (current_star >= 2)
		{
			star2tex.texture = emptyStar;
			star1tex.texture = filledStar;
			star0tex.texture = filledStar;
		}
		else if (current_star >= 1)
		{
			star2tex.texture = emptyStar;
			star1tex.texture = emptyStar;
			star0tex.texture = filledStar;
		}
		else if (current_star == 0)
		{
			star2tex.texture = emptyStar;
			star1tex.texture = emptyStar;
			star0tex.texture = emptyStar;
		}
	}

	private void increaseStar()
	{
        audioSource.PlayOneShot(starWinSound, 0.5f);
		current_star++;
		updateStars();
	}

	private void decreaseStar()
    {
        audioSource.PlayOneShot(starLossSound, 0.5f);
        current_star--;
		updateStars();
	}

	private int get_current_star()
	{
		int index = 0;
		foreach (int star in star_threshold_list)
		{
			if (playerInfo.Loot() < star)
			{
				break;
			}

			index++;
		}

		return index;
	}

	private void EliminateAllEnemies()
	{
		var objects = GameObject.FindGameObjectsWithTag("Enemy");
		foreach (GameObject obj in objects)
		{
			if(explosion != null)
				Instantiate(explosion, obj.transform.position, obj.transform.rotation);
			Destroy(obj);
		}
	}

	private void DisablePlayer()
	{
		playerInfo.gameObject.SetActive(false);
	}

	private void FixedUpdate()
	{
		if (timer.GetLeftOverTime() <= 0 || gameManager.GetPlayerInfo().IsDead())
		{
			//we have finished the current level
			if (current_star > 0)
			{
				//Level finished in victory
				endText.text = "VICTORY";
				endText.gameObject.SetActive(true);
				ExitButton.SetActive(true);
				var nextLevel = gameManager.NextLevel();
				if (nextLevel < SceneManager.sceneCountInBuildSettings)
					NextLevelButton.SetActive(true);
				RestartButton.SetActive(true);
			}
			else
			{
				//Level finished in defeat
				endText.text = "DEFEAT - NO STARS";
				endText.gameObject.SetActive(true);
				ExitButton.SetActive(true);
				RestartButton.SetActive(true);
			}
			EliminateAllEnemies();
			DisablePlayer();
		}
	}

	// Update is called once per frame
	void OnGUI () {
		int new_star = get_current_star();

		if (new_star > current_star) increaseStar();
        if (new_star < current_star) decreaseStar();
    }
}
