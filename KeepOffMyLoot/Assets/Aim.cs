﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aim : MonoBehaviour {
    
    private Camera main_camera;
    private GameObject player;
	public Vector3 target;

	// Use this for initialization
	void Start () {
        main_camera = GameObject.FindWithTag("MainCamera").GetComponent<Camera>();
		player = GameObject.FindWithTag("Player");
		target = transform.position + Vector3.forward;
	}
	
	// Update is called once per frame
	void LateUpdate ()
    {
        Plane player_plane = new Plane(Vector3.up, player.transform.position);
		var mousePosition = Input.mousePosition;
		mousePosition = new Vector3(mousePosition.x, mousePosition.y, 0f);
        Ray ray = main_camera.ScreenPointToRay(mousePosition);
        float rayDistance;
		
        if (player_plane.Raycast(ray, out rayDistance))
        {
            target = ray.GetPoint(rayDistance);
			var forward = target - player.transform.position;
			forward.Normalize();
			transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(forward, Vector3.up), 50);
			Debug.Log(ray);
		}
    }
}
