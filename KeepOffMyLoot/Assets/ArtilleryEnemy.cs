﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArtilleryEnemy : Enemy {

    public Transform pointShoot;

    protected override IEnumerator shoot(float countdownValue)
    {
        canShoot = false;
        if (AlwaysShootStraight && pathAgent.target != null)
        {
            Instantiate(Projectile, transform.position, Quaternion.LookRotation((pathAgent.target.position - transform.position).normalized, Vector3.up));
        }
        else
            Instantiate(Projectile, pointShoot.position, pointShoot.rotation);

        yield return new WaitForSeconds(countdownValue);

        canShoot = true;
    }
}
