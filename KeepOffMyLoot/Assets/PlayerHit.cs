﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHit : MonoBehaviour {

	PlayerInfo playerInfo;
	public float BlinkTime = 0.1f;
	bool canBeHurt = true;
	Material currentPlayerMat;

	private IEnumerator blink(float countdownValue)
	{
		var countdownIteration = countdownValue / 3.0f;
		var currentColor = currentPlayerMat.color;
		canBeHurt = false;
		currentPlayerMat.color = Color.red;
		yield return new WaitForSeconds(countdownIteration);
		currentPlayerMat.color = currentColor;
		yield return new WaitForSeconds(countdownIteration);
		currentPlayerMat.color = Color.red;
		yield return new WaitForSeconds(countdownIteration);
		currentPlayerMat.color = currentColor;
		canBeHurt = true;
	}

	// Use this for initialization
	void Start () {
		playerInfo = transform.GetComponent<PlayerInfo>();
		currentPlayerMat = transform.GetComponentInChildren<MeshRenderer>().material;
	}

	public void blink()
	{
		if (canBeHurt)
		{
			StartCoroutine(blink(BlinkTime));
		}
	}
}
