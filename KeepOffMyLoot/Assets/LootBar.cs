﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LootBar : MonoBehaviour
{

	private PlayerInfo player;
	public Transform loot_bar;
    public Text lootText;

    void Awake()
	{
		player = GameObject.FindWithTag("Player").GetComponent<PlayerInfo>();
	}

	void OnGUI()
	{
		loot_bar.localScale = new Vector3(Mathf.Min(100, player.Loot()) / 100.0f, loot_bar.localScale.y, loot_bar.localScale.z);
        lootText.text = player.Loot().ToString();
	}
}
