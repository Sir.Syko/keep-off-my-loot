﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorController : MonoBehaviour {

	Texture2D cursor;
	int cursorSizeX = 11;
	int cursorSizeY = 11;
	GameObject playerPivot;
	
	// Use this for initialization
	void Start () {
		Cursor.visible = false;
		playerPivot = GameObject.Find("Player").transform.Find("Pivot").gameObject;
		cursor = Resources.Load("Images/cursor") as Texture2D;
		//Cursor.SetCursor(Resources.Load("Images/cursor") as Texture2D, new Vector2(0.5f, 0.5f), CursorMode.ForceSoftware);
	}

	// Update is called once per frame
	void OnGUI() {
		Camera.main.WorldToScreenPoint(playerPivot.transform.position);
		var angle = playerPivot.transform.rotation.eulerAngles;
		var matx = GUI.matrix;
		var x = Event.current.mousePosition.x - cursorSizeX / 2.0f;
		var y = Event.current.mousePosition.y - cursorSizeY / 2.0f;
		var pivot = new Vector2(x + cursorSizeX / 2.0f, y + cursorSizeY / 2.0f);
		GUIUtility.RotateAroundPivot(angle.y, pivot);
		GUI.DrawTexture(new Rect(x, y, cursorSizeX, cursorSizeY), cursor);
		GUI.matrix = matx;
	}
}
