﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

	public float Speed = 3;
	public int Damage = 1;
	public float LifeTime = 2.5f;
	protected float currTime = 0f;

	public GameObject Target;
	public GameObject Explosion;

	// Use this for initialization
	void Start ()
	{
		
	}

	protected virtual void HandleDeath()
	{
		if(Explosion != null)
			Instantiate(Explosion, transform.position, transform.rotation);
		Destroy(this.gameObject);
	}

	protected virtual void OnCollisionEnter(Collision collision)
	{
		ContactPoint contact = collision.contacts[0];
		Quaternion rot = Quaternion.FromToRotation(Vector3.up, contact.normal);
		Vector3 pos = contact.point;

		// we are a player projectile
		if (transform.tag.CompareTo("Projectile") == 0)
		{
			if (collision.transform.tag.CompareTo("Enemy") == 0)
			{
				var enemyScript = collision.transform.GetComponent<Enemy>();
				enemyScript.ReceiveDamage(Damage);
				HandleDeath();
			}
		}

		// we are a enemy projectile
		if (transform.tag.CompareTo("EnemyProjectile") == 0)
		{
			if (collision.transform.tag.CompareTo("Player") == 0)
			{
				var playerScript = collision.transform.GetComponent<PlayerInfo>();
				playerScript.hurtPlayer(Damage);
				HandleDeath();
			}

            if (collision.transform.tag.CompareTo("Chest") == 0)
            {
                var playerScrpt = GameObject.FindWithTag("Player").GetComponent<PlayerInfo>();
                playerScrpt.LoseLoot(Damage);
				HandleDeath();
			}
        }

		if (collision.transform.tag.CompareTo("Obstacle") == 0)
		{
			HandleDeath();
		}
    }

	protected virtual void FixedUpdate()
	{
		currTime += Time.fixedDeltaTime;

		if (currTime > LifeTime)
			HandleDeath();
		if (Target != null)
		{
			var targetPosition = new Vector3(Target.transform.position.x, 0f, Target.transform.position.z);
			transform.position = Vector3.MoveTowards(transform.position, targetPosition, 0.1f * Speed);
			transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(targetPosition - transform.position, Vector3.up), 50);
		}
		else
		{
			transform.position = Vector3.MoveTowards(transform.position, transform.position +  transform.forward * 3.0f, 0.1f * Speed);
		}
	}
}
